<div>
	<form method="get">
		<div><label>Correo electronico: </label><input type="text" name="email"></div>
		<div><label>Contrase&ntilde;a: </label><input type="password" name="password"></div>
		<div>
			<label>Veces a Ejecutar (Singin and Questions): </label>
			<input type="text" value=0 name="numRedirct">
		</div>
		<div><input type="submit" value="Probar servidor"></div>
	</form>
</div>

<?php

#Esto ayuda ha poder ver los errores apesar de que elstatus del servidor es de 500
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

#Api que contendra el manejo de los datos
include('getApi.php');

if(isset($_GET['numRedirct']))
{
  $nprocess = array();
  $init = 1;
  $end = (int) $_GET['numRedirct'];
    
  # Proccess Fork
  for($i = $init; $i <= $end; $i++)
  {
    $pid = pcntl_fork();
    
    if($pid == -1)
      die("No Esta Funcionando el multiproceso...");
    
    elseif($pid)
    {
      $nprocess[] = $pid;
    }
    
    else
    {
      exec_server($i);
      die();
    }
  }
  
  foreach($nprocess as $pid)
  {
      pcntl_waitpid($pid, $status);
  }
}


# esta funcion ejecutara las llamadas al servidor
function exec_server($id)
{
  print("<div><h3 style='text-align:center'>SingIn({$id})</h3>");
  $singIn = singinAction($_GET['email'], $_GET['password']);
  print("</div><br><br>");
  print("<div><h3 style='text-align:center'>Questions({$id})</h3>");
  $Questions = getQuestions($singIn['header']);
}

#Action de iniciar cesión
function singinAction($email, $password)
{
	#datos de la pagina
	$page = array(
		'uri' => 'https://api.sports4u-quiz.com/auth/sign_in',
		'auth_bool' => false,
		'header' => true
	);

	#parametros
	$body = array(
		'email' => $email,
		'password' => $password
	);

	#Instancia que se comunicara con la pagina externa
	$openApi = new Application_Model_getApi();
	$openApi->setConfig($page);
	$openApi->openApi($body);
	return array('response' => $openApi->getDataResponse(), 'header' => $openApi->getHeaders());
}

#hace la consulta del end-point
function getQuestions($auth = array())
{
	#direccion de la pagina y sus datos si quiero header pero no hay confirmacion de una header de autorización...
	$page = array(
		'uri' => 'https://api.sports4u-quiz.com/questions/get_questions',
		'auth_bool' => true,
		'auth_headers' => $auth,
		'header' => true
	);

	#datos a enviar
	$body = array(
		'category_id' => '',
		'level_id' => '1'
	);

	$openApi = new Application_Model_getApi();
	$openApi->setConfig($page);
	$openApi->openApi($body);
	return array('response' => $openApi->getDataResponse(), 'header' => $openApi->getHeaders());
}